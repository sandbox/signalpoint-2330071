<?php

/**
 * Implements hook_views_query_alter().
 */
function commerce_deposit_views_query_alter(&$view, &$query) {
  // Since Drupal Commerce by default doesn't consider line items that don't
  // reference a product when rendering the line items, we need to include our
  // custom line item types in the where clause conditions of various Views.
  //drupal_set_message($view->name);
  //dpm($query);
  switch ($view->name) {
    case 'commerce_cart_summary':
    case 'commerce_cart_form':
    case 'commerce_cart_block':
    case 'commerce_line_item_table':
      if ($view->current_display == 'default') {
        // Iterate over all where clauses and conditions until we find the one
        // we're looking for.
        $found = FALSE;
        foreach ($query->where as $index => $where) {
          if (!isset($where['conditions']) || empty($where['conditions'])) { continue; }
          foreach ($where['conditions'] as $delta => $condition) {
            if (
              in_array($condition['field'], array(
                'commerce_line_item_field_data_commerce_line_items.type',
                'commerce_line_item.type'
              ))
            ) {
              $found = TRUE;
              foreach (commerce_deposit_valid_line_item_types() as $line_item_type) {
                $query->where[$index]['conditions'][$delta]['value'][] = $line_item_type;
              }
              break;
            }
          }
          if ($found) { break; }
        }
      }
      break;
  }
}

/**
 * Implements hook_views_data()
 */
function commerce_deposit_views_data() {
  $data = array();
  // Adds a button to pay the balance due on a deposit line item.
  $data['commerce_line_item']['pay_balance'] = array(
    'field' => array(
      'title' => t('Commerce Deposit: Pay balance'),
      'help' => t('Adds a button to pay the balance on a deposit.'),
      'handler' => 'commerce_deposit_handler_field_pay_balance',
    ),
  );
  return $data;
}

