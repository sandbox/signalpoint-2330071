<?php

/**
 * @file
 * Field handler to present a button to pay a balance on a deposit line item.
 */

/**
 * Field handler to present a button to pay a balance on a deposit line item.
 */
class commerce_deposit_handler_field_pay_balance extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['line_item_id'] = 'line_item_id';

    // Set real_field in order to make it generate a field_alias.
    $this->real_field = 'line_item_id';
  }

  function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * Returns the form which replaces the placeholder from render().
   */
  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $form[$this->options['id']] = array(
      '#tree' => TRUE,
    );
    // At this point, the query has already been run, so we can access the results
    // in order to get the base key value (for example, nid for nodes).
    foreach ($this->view->result as $row_id => $row) {
      $line_item_id = $this->get_value($row);

      $form[$this->options['id']][$row_id] = array(
        '#type' => 'submit',
        '#value' => t('Pay balance'),
        '#name' => 'pay-balance-line-item-' . $row_id,
        '#attributes' => array('class' => array('pay-balance-line-item')),
        '#line_item_id' => $line_item_id,
        '#submit' => array_merge($form['#submit'], array('commerce_deposit_line_item_views_pay_balance_form_submit')),
      );
    }
  }

  function views_form_submit($form, &$form_state) {
    $field_name = $this->options['id'];
    foreach (element_children($form[$field_name]) as $row_id) {
      if ($form_state['triggering_element']['#name'] == 'pay-balance-line-item-' . $row_id) {
        $line_item_id = $form[$field_name][$row_id]['#line_item_id'];
        commerce_deposit_balance_generate_order($line_item_id, $form, $form_state);
      }
    }
  }

}
