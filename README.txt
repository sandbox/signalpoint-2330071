Commerce Deposit
================

# Module Installation and Setup

Download and enable the module as usual. Then read the sections below...

# How it Works

When enabled, this module automatically creates a custom line item type called
Deposit. You can see it here:

  Store -> Configuration -> Line item types
  (admin/commerce/config/line-items)

The site administrator then specifies which product types will have the
deposit feature enabled.

  Store -> Deposits -> Configuration
  (admin/commerce/deposits/config)
  
The site administrator then specifies which products will have desposits
enabled, and how much the deposit costs.

  Store -> Products
  (admin/commerce/products)

To set up a deposit on a product, follow these steps:

1. Click "edit" next to a product, whose product type is enabled for deposits
2. Check the box next to "Deposit enabled"
3. Enter a price into the "Deposit amount" field
4. Click "Save Product"
5. Repeat this process for each product that needs a deposit

Note, consider using the Views Bulk Operations module if you need to enable
deposits across many products.

# Adding a Deposit to an Order

When a customer adds a deposit line item to their cart, it will reference the
product for which it is making the deposit against. Note, this reference occurs
with an entity reference, not a product reference.

# For Site Builders and Developers to Consider

It is important to understand that Drupal Commerce, by default, does not
count custom line items that do not have a product reference when reporting
how many items are in the cart. 

This module overrides the default empty cart block and page, by including
the commerce_deposit line item type when determining how many items are in the
cart. It then uses a cloned copy of the following two default Commerce views to
render the block and page, respectively:

 - commerce_cart_block
 - commerce_cart_form

The only modification on these views from its original copy, is adding an "OR"
filter to include deposit line item types in the results.

When a deposit line item is added to an order, it references the corresponding
product via an Entity Refernce field, not via a Product Reference field. We do
this because we don't want Drupal Commerce (or others) thinking a deposit is a
product, because it isn't, but it must reference the product in some way.

